package com.mygdx.game.Tools;

import com.mygdx.game.AgarIOGame;
import com.mygdx.game.screens.CreditScreen;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.MenuScreen;

public class ScreenManager {
    private  static ScreenManager screenManager;
    private AgarIOGame agarIOGame;

    private ScreenManager(AgarIOGame agarIOGame){
        this.agarIOGame = agarIOGame;
    }
    public static ScreenManager getScreenManager(AgarIOGame agarIOGame){
        if(screenManager==null){
            screenManager=new ScreenManager(agarIOGame);
        }
        return  screenManager;
    }
    public MenuScreen getMenuScreen() {
        return new MenuScreen(agarIOGame);
    }

    public GameScreen getGameScreen() {
        return new GameScreen(agarIOGame);
    }

    public CreditScreen getCreditsScreen() {
        return new CreditScreen(agarIOGame);
    }


 }
