package com.mygdx.game.components.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;

public class CircleComponent implements Component {
    public Color color;
    public float radiusOfCircle;
}
