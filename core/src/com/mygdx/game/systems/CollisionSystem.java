package com.mygdx.game.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.audio.Sound;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.Tools.Mapers;
import com.mygdx.game.components.components.CircleComponent;
import com.mygdx.game.components.components.PositionComponent;

import com.mygdx.game.components.components.VelocityComponent;


public class CollisionSystem extends EntitySystem {

    private AgarIOGame game;
    private ImmutableArray<Entity> dynamicMembers;
    private ImmutableArray<Entity> allEntites;
    private Entity player;
    private Sound hitSound;

    public CollisionSystem(AgarIOGame game, PlayerControllSystem playerControllSystem) {
        this.game = game;
        player = playerControllSystem.getPlayer();
        allEntites = game.getGameEngine().getEntities();
        dynamicMembers = game.getGameEngine().getEntitiesFor(Family.all(PositionComponent.class, CircleComponent.class, VelocityComponent.class).get());
        hitSound = game.getAssetManager().get("sound.ogg");
    }

    @Override
    public void update(float deltaTime) {
        checkCollisions();

    }

    //check if first circle intersects with second
    private void checkCollisionBeetwenTwoEntities(Entity firstEntity, Entity secondEntity) {
        CircleComponent firstCircle = Mapers.clm.get(firstEntity);
        CircleComponent secondCircle = Mapers.clm.get(secondEntity);
        PositionComponent firstEnPos = Mapers.pm.get(firstEntity);
        PositionComponent secondEnPos = Mapers.pm.get(secondEntity);
        //calculating distance between two entities
        float distance = game.calculateDistanceBetweenPoints(firstEnPos.posx, firstEnPos.posy, secondEnPos.posx, secondEnPos.posy);
        if (firstCircle.radiusOfCircle + secondCircle.radiusOfCircle >= distance) {
            hitSound.play(0.1f);
            if (firstCircle.radiusOfCircle < secondCircle.radiusOfCircle) {

                if (checkForPlayer(firstEntity)) {
                    game.isGameEnded=true;
                }else {
                    grow(secondEntity, firstEntity);
                    game.getGameEngine().removeEntity(firstEntity);
                }
            } else {
                if (checkForPlayer(secondEntity)) {
                    game.isGameEnded=true;
                }else {
                    grow(firstEntity, secondEntity);
                    game.getGameEngine().removeEntity(secondEntity);
                }
            }
        }

    }


    private void grow(Entity eater, Entity swallowedEntity) {
        CircleComponent firstCircle = Mapers.clm.get(eater);
        CircleComponent secondCircle = Mapers.clm.get(swallowedEntity);

        float firstCircleRadius = firstCircle.radiusOfCircle;
        float secondCircleRadius = secondCircle.radiusOfCircle;
        // calculating area for new radius
        double area = Math.PI * firstCircleRadius * firstCircleRadius + Math.PI * secondCircleRadius * secondCircleRadius;
        double newRadius = Math.sqrt(area / Math.PI);

        firstCircle.radiusOfCircle = (float) newRadius;
    }

    private void checkCollisions() {
        //all dynamicEntities

        for (Entity dynamicEntity : dynamicMembers) {
            // allEntities
            for (Entity entityFromWorld : allEntites) {
                if (dynamicEntity != entityFromWorld) {
                    checkCollisionBeetwenTwoEntities(dynamicEntity, entityFromWorld);
                }
            }
        }
    }

    private boolean checkForPlayer(Entity entity) {
        return entity == player;
    }
}


