package com.mygdx.game.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.AgarIOGame;

public class CreditScreen extends DefAbstractScreen {
    private String info;
    private Label label;
    private ImageButton backBtn;

    public CreditScreen(AgarIOGame agarIOGame) {
        super(agarIOGame);
        info = " author-LimesInf \n Designer ........  \n sound effects-Google\n Game idea - (AVIK,Gev,Davit, Maybe Hayk :P) ";
        label = new Label(info, style);
        backBtn = new ImageButton(new TextureRegionDrawable(game.getAtlas().findRegion("back")));
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(game.getScreenManager().getMenuScreen());
            }
        });
        table.add(label);
        table.row();
        table.add(backBtn).size(90, 90);
    }
}
