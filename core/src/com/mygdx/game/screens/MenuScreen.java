package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.AgarIOGame;

public class MenuScreen extends DefAbstractScreen {
    private ImageButton playBtn;
    private ImageButton creditsBtn;
    private ImageButton exitBtn;
    private Music music;

    public MenuScreen(AgarIOGame agarIOGame) {
        super(agarIOGame);
        creditsBtn = new ImageButton(new TextureRegionDrawable(game.getAtlas().findRegion("credits")));
        creditsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(game.getScreenManager().getCreditsScreen());
            }
        });
        playBtn = new ImageButton(new TextureRegionDrawable(game.getAtlas().findRegion("play")));
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(game.getScreenManager().getGameScreen());
            }
        });
        exitBtn=new ImageButton(new TextureRegionDrawable(game.getAtlas().findRegion("quit")));
        exitBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        table.add(playBtn).size(90, 90);
        table.row();
        table.add(creditsBtn).size(90, 90);
        table.row();
        table.add(exitBtn).size(90,90);
        music=agarIOGame.getAssetManager().get("music.mp3");
        music.setVolume(0.7f);
        music.setLooping(true);
        music.play();
    }
}
